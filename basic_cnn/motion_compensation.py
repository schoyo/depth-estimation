import numpy as np
import glob
from os import path, mkdir

eps = 0.02
t_offset = 0.1 #100ms for 10Hz

def transform(v, w, dt):
    theta = w * dt
    if abs(w)<eps: #accounting for small angular velocity
        dx = v * (dt - w**2 * dt**3 / 6 + w**4 * dt**5 / 120) #first 3 values of taylor series expansion
        dy = v * (w * dt**2 / 2 - w**3 * dt**4 / 24)
    else:
        r = v/w
        dx = r*np.sin(theta)
        dy = r*(1-np.cos(theta))
    T = np.array((dx,dy,0)).reshape(-1,1)
    R = rotz(-theta)
    return R, T

def rotz(theta):
    c, s = np.cos(theta), np.sin(theta)
    R = np.array([[c, -s, 0],
                [s, c, 0],
                [0, 0, 1]])
    return R

def _load_timestamps(folder_path, frames=None):
    """Load timestamps from file."""
    timestamp_file = folder_path 

    # Read and parse the timestamps
    timestamps = []
    with open(timestamp_file, 'r') as f:
        for line in f.readlines():
            timestamps.append(float(line))

    # Subselect the chosen range of frames, if any
    if frames is not None:
        timestamps = [timestamps[i] for i in frames]

    return timestamps

def _load_velocities(folder_path, frames=None):
    """Load velocities from file."""
    velocity_file = folder_path + '/velocity.txt'

    # Read and parse the timestamps
    v = []
    w = []
    with open(velocity_file, 'r') as f:
        for line in f.readlines():
            vals = line.split(",")
            v.append(float(vals[0]))
            w.append(float(vals[3]))

    # Subselect the chosen range of frames, if any
    if frames is not None:
        v = [v[i] for i in frames]
        w = [w[i] for i in frames]

    v, w = np.array(v), np.array(w)

    #convert to SI units
    v /= 3.6 #km/h to m/s
    w *= np.pi/180 #deg/s to rad/s

    return v, w

def _load_lidar(folder_path, idx):
    lidar_path = folder_path[idx]
    lidar = np.fromfile(lidar_path, dtype=np.float32)
    lidar = lidar.reshape((-1, 8))
    lidar = np.transpose(lidar[:,0:3])

    return lidar

#load data
folder_path = "/home/mcav/Localisation/slam/depth_labelling/set3-2"
new_folder = '/lidar_corrected'
cam_times = _load_timestamps(folder_path+'/times_cam.txt')
lidar_times = _load_timestamps(folder_path+'/times_lidar.txt')
linear_velocity, angular_velocity = _load_velocities(folder_path)
velo_files = sorted(glob.glob(folder_path+'/lidar/*.bin'))

if not path.exists(folder_path+new_folder):
        mkdir(folder_path+new_folder)

for frame in range(len(velo_files)):
    lidar = _load_lidar(velo_files, frame)
    t_cam, t_lidar, v, w = cam_times[frame], lidar_times[frame], linear_velocity[frame], angular_velocity[frame]

    #transform for camera and lidar time offset
    t_cam_lidar = t_cam - t_lidar
    R, T = transform(v, w, t_cam_lidar)
    lidar_cam = np.matmul(R,(lidar-T))

    #calculate time offset for each lidar point by angle
    angle = np.arctan2(lidar_cam[1,:],lidar_cam[0,:])
    angle[angle<0] += 2*np.pi #[0,2pi]
    dt_angle = (1-angle/(2*np.pi))*t_offset

    #transform
    for i in range(len(dt_angle)):
        R, T = transform(v, w, dt_angle[i])
        lidar_cam[:,i] = np.matmul(R,(lidar_cam[:,i].reshape(-1,1)-T)).squeeze()

    lidar_cam.astype('float32').tofile('{}{}/{:05d}.bin'.format(folder_path,new_folder,frame+1))