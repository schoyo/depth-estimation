# README for Depth Estimation Neural Network and Sensor Calibration
## Depth Neural Network 
Configuration files to be used with `main.py`
* `config.yaml` - kitti dataset
* `config_sd.yaml` - streetdrone dataset

Training and testing inputs
*  Associated files for KITTI Eigen split training/testing and those used for StreetDrone (not all StreetDrone files are included)

Components
* `main.py` - script for training, evaluating and predicting model
* `model.py` - final architecture used in project 
* `dataloader.py` - used for loading and pre-processing inputs
* `evaluation.py` - all functions associated with evaluation and prediction including metrics

Models
*  All models can be found [here](https://drive.google.com/drive/u/1/folders/1PDZ5HijXwl2EP603kNvGf0i0UzAbze8N)

## Sensor Calibration Network
Configuration files
* `config_kitti_cam_calib.yaml` - kitti dataset
* `config_sd_cam_calib.yaml` - streetdrone dataset

## Components
* `cam_calib_nn.py` - standalone calibration correction code (can be split into separate files as above network for clarity), can be used with motion compensation
* `motion_compensation.py` - corrects LiDAR data within a folder using motion compensation and time synchronisation

## Integrated Network
* `config_sd_cam_calib_motion.py` - streetdrone dataset
* `cam_calib_motion_nn.py` - integrated motion compensation, time synchronisation and sensor calibration (runs quite slowly)

## Other
* `folder_tools.py` - useful tools written throughout the project for processing data in bulk, includes script to create association files for training 