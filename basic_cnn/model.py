import torch
import torch.nn as nn
import torch.nn.functional as F
import math

class log_loss(nn.Module):
    def __init__(self):
        super(log_loss, self).__init__()

    def forward(self, depth_est, depth_gt, mask):
        d = torch.log(depth_est[mask]) - torch.log(depth_gt[mask])
        return (d**2).mean() 

class inv_huber_loss(nn.Module):
    def __init__(self):
        super(inv_huber_loss, self).__init__()

    def forward(self, depth_est, depth_gt, mask):
        absdiff = torch.abs(torch.log(depth_est[mask]) - torch.log(depth_gt[mask]))
        C = 0.2*torch.max(absdiff).item()
        return torch.mean(torch.where(absdiff < C, absdiff,(absdiff*absdiff+C*C)/(2*C) ))

class tolerance_loss(nn.Module):
    def __init__(self):
        super(tolerance_loss, self).__init__()

    def forward(self, depth_est, depth_gt, mask):
        eps = 1e-10
        windows = F.unfold(depth_est, kernel_size=3,padding=1)
        depth_gt_reshaped = torch.flatten(depth_gt,start_dim=2)
        d2=(torch.log(windows+eps)-torch.log(depth_gt_reshaped+eps))**2
        values,indices=d2.min(dim=1)
        loss=torch.reshape(values,depth_gt.shape)
        return loss[mask].mean()  

class neighbour_inv_huber_loss(nn.Module):
    def __init__(self):
        super(neighbour_inv_huber_loss, self).__init__()

    def forward(self, depth_est, depth_gt, mask):
        eps = 1e-10
        windows = F.unfold(depth_est, kernel_size=3,padding=1)
        depth_gt_reshaped = torch.flatten(depth_gt,start_dim=2)

        absdiff_n = torch.abs(torch.log(windows+eps)-torch.log(depth_gt_reshaped+eps))
        values,indices=absdiff_n.min(dim=1)
        absdiff=torch.reshape(values,depth_gt.shape)[mask]

        C = 0.2*torch.max(absdiff).item()
        return torch.mean(torch.where(absdiff < C, absdiff,(absdiff*absdiff+C*C)/(2*C) ))

class Decoder(nn.Module):
    def conv_block(self, in_channels, out_channels, kernel_size=3):
        block = nn.Sequential(
            nn.Conv2d(in_channels,out_channels, kernel_size=kernel_size, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(),
            nn.Conv2d(out_channels,out_channels, kernel_size=kernel_size, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
            nn.ReLU()
        )
        return block

    def conv_block_single(self, in_channels, out_channels, kernel_size=3):
        block = nn.Sequential(
            nn.Conv2d(in_channels,out_channels, kernel_size=kernel_size, stride=1, padding=1),
            nn.BatchNorm2d(out_channels),
            nn.ReLU()
        )
        return block

    def downsample(self, in_channels, out_channels, kernel_size=4, stride=2):
        block = nn.Sequential(
            nn.Conv2d(in_channels,out_channels, kernel_size=kernel_size, stride=stride, padding=1),
            nn.BatchNorm2d(out_channels),
            nn.ReLU()
        )
        return block

    def upsample(self, in_channels, out_channels, kernel_size=4):
        block = nn.Sequential(
            nn.ConvTranspose2d(in_channels,out_channels, kernel_size=kernel_size, stride=2, padding=1),
            nn.BatchNorm2d(out_channels),
            nn.ReLU()
        )
        return block

    def __init__(self):
        super(Decoder, self).__init__()
        feat_out_channels = [96, 96, 192, 384, 2208] #densenet161
        # feat_out_channels = [64, 64, 128, 256, 1024] #densenet121
        num_features = 512
        self.offset = nn.Parameter(torch.rand(1)) #trainable parameter to account for focal length offset
        
        #Decode
        self.upsample4 = self.upsample(feat_out_channels[4], num_features)
        self.conv_decode4 = self.conv_block_single(num_features + feat_out_channels[3], num_features)
        
        self.upsample3 = self.upsample(num_features, num_features//2)
        self.conv_decode3 = self.conv_block_single(num_features//2 + feat_out_channels[2], num_features//2)
        self.conv3 = self.conv_block_single(num_features//2 + feat_out_channels[2], 1)
        
        self.upsample2 = self.upsample(num_features//2, num_features//4)
        self.conv_decode2 = self.conv_block_single(num_features//4 + feat_out_channels[1], num_features//4)
        self.conv2 = self.conv_block_single(num_features//4 + feat_out_channels[1], 1)
        
        self.upsample1 = self.upsample(num_features//4, num_features//8)
        self.conv_decode1 = self.conv_block_single(num_features//8 + feat_out_channels[0], num_features//8)
        self.conv1 = self.conv_block_single(num_features//8 + feat_out_channels[0], 1)
        
        self.upsample0 = self.upsample(num_features//8, num_features//16)
        self.conv0 = self.conv_block_single(num_features//16, 1)
        
        self.depth_est = nn.Conv2d(num_features,1, kernel_size=3, stride=1, padding=1)
    
    def forward(self, features):
        skip1, skip2, skip3, skip4, dense_features = features[3], features[4], features[6], features[8], features[11]

        #Decode
        upsample4 = self.upsample4(dense_features)
        concat4 = torch.cat([upsample4,skip4], dim=1)
        conv_decode4 = self.conv_decode4(concat4)
        depth4 = self.depth_est(conv_decode4)

        upsample3 = self.upsample3(conv_decode4)
        concat3 = torch.cat([upsample3,skip3], dim=1)
        conv_decode3 = self.conv_decode3(concat3)
        depth3 = torch.add(F.interpolate(depth4,scale_factor=2,mode='bilinear'), self.conv3(concat3))

        upsample2 = self.upsample2(conv_decode3)
        concat2 = torch.cat([upsample2,skip2], dim=1)
        conv_decode2 = self.conv_decode2(concat2)
        depth2 = torch.add(F.interpolate(depth3,scale_factor=2,mode='bilinear'), self.conv2(concat2))
        
        upsample1 = self.upsample1(conv_decode2)
        concat1 = torch.cat([upsample1,skip1], dim=1)
        conv_decode1 = self.conv_decode1(concat1)
        depth1 = torch.add(F.interpolate(depth2,scale_factor=2,mode='bilinear'), self.conv1(concat1))

        upsample0 = self.upsample0(conv_decode1)
        depth0 = torch.add(F.interpolate(depth1,scale_factor=2,mode='bilinear'), self.conv0(upsample0))

        #Final
        final = torch.add(depth0, self.offset)
        out = torch.exp(final)

        return out

class Encoder(nn.Module):
    def __init__(self):
        super(Encoder, self).__init__()       
        import torchvision.models as models
        self.original_model = models.densenet161( pretrained=True )
        # self.original_model = models.densenet121( pretrained=True )

    def forward(self, x):
        features = [x]
        for k, v in self.original_model.features._modules.items(): features.append( v(features[-1]) )
        return features

class Network(nn.Module):
    def __init__(self):
        super(Network, self).__init__()
        self.encoder = Encoder()
        self.decoder = Decoder()

    def forward(self, x):
        features = self.encoder(x)
        return self.decoder(features)