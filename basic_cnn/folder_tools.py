import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2 
from PIL import Image
import glob
import numpy as np
from os import path,mkdir

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

#used to specify folder associations for neural network data loading 
def file_association(file_paths, prefixes, filename = 'test'):
    fid = open(filename+'.txt', 'w')
    files = []
    for i in range(len(file_paths)):
        files.append(sorted(glob.glob(file_paths[i]+'/*')))

    for i in range(len(files[0])):
        for j in range(len(file_paths)):
            f = remove_prefix(files[j][i],prefixes[j])
            fid.write('{} '.format(f))
        
        fid.write('\n')


#creates verification images from depth and rectified images - assumes folder structure
def verification(folder_path):
    depth_list = []
    image_list = []

    for filename in sorted(glob.glob(folder_path+'/depth/*.png')): 
        im=Image.open(filename)
        depth_list.append(im)

    for filename in sorted(glob.glob(folder_path+'/rectified_images/*.png')): 
        im=cv2.imread(filename)
        image_list.append(im)

    if not path.exists(path.dirname(folder_path+"/verification/")):
        mkdir(folder_path+"/verification")

    for i in range(len(image_list)):
        combined = image_list[i]
        depth_array = np.asarray(depth_list[i], dtype=np.float32) 
        depth_array = np.expand_dims(depth_array, axis=2) / 256.0

        depth_img = np.array(depth_array * 255/depth_array.max(), dtype = np.uint8) #visual verification 
        depth_map = cv2.applyColorMap(depth_img, cv2.COLORMAP_JET)

        # combine images
        mask = (depth_array>0).squeeze()
        combined[mask]=depth_map[mask]           
        cv2.imwrite(folder_path+"/verification/{:05d}.png".format(i+1), combined)

#rectifies all images in folder path
def rectify(folder_path, camera_intrinsics, distortion_coeffs, scale=1.0):
    image_list = []

    for filename in sorted(glob.glob(folder_path+'/raw/*.png')): 
        im=cv2.imread(filename)
        image_list.append(im)
    print("Loaded files")
    if not path.exists(path.dirname(folder_path+"/rectified/")):
        mkdir(folder_path+"/rectified")
    
    for i in range(len(image_list)):
        undistort_img = cv2.undistort(image_list[i], camera_intrinsics, distortion_coeffs) # Undistort image
        undistort_img = cv2.resize(undistort_img, (0,0), fx = scale, fy = scale) # Resize image by scale factor

        cv2.imwrite(folder_path+"/rectified/{:05d}.png".format(i+1), undistort_img)

# Usage examples
# training dataset file associations for depth nn
sets = ["2020-02-24-14-09-23","2020-02-24-14-12-34","2020-03-05-13-52-19","2020-03-05-13-54-12","2020-03-06-14-50-52"]
for i in range(len(sets)):
    fp = "/home/mcav/DATA/streetdrone_dataset/new/{}/".format(sets[i])
    fp2 = "/home/mcav/Depth_Estimation/basic_cnn/raw/set{}/".format(i+1)
    pf = '/home/mcav/DATA/streetdrone_dataset/new/'
    pf2 = "/home/mcav/Depth_Estimation/basic_cnn/raw/"
    file_association([fp + "lidar",fp2, fp + "rectified_images"],[pf, pf2, pf],'set{}_train'.format(i+1))

# verification images
for i in range(len(sets)):
    fp = "/home/mcav/DATA/streetdrone_dataset/{}".format(sets[i])
    verification(fp)

# training dataset file associations for calib nn
name = "set5-2/"
fp = "/home/mcav/Localisation/slam/depth_labelling/"
fp2 = "/home/mcav/Depth_Estimation/basic_cnn/raw/"
file_association([fp+name+"lidar",fp+name+"depth",fp+name+"rectified_images"],[fp,fp,fp],'motion_set5-2')

# training dataset file associations for calib nn
sets = ["2020-02-24-14-09-23","2020-02-24-14-12-34","2020-03-05-13-54-12"]
for i in range(len(sets)):
    fp = "/home/mcav/DATA/streetdrone_dataset/fixed/{}/".format(sets[i])
    pf = '/home/mcav/DATA/streetdrone_dataset/fixed/'
    file_association([fp + "rectified_images",fp + "depth_corrected"],[pf, pf],'set{}_fixed'.format(i+1))

# rectification of images
fp = "/home/mcav/DATA/streetdrone_dataset/new/2020-03-05-13-54-12"
cam_in = np.array([[977.670543504134,0.000000,952.293908430118],[0.000000,976.462005075034,638.588227956366],[0.000000,0.000000,1.000000]])
dist_coeff = np.array([-0.313078796637538,0.0910010605836340,-0.000128613926876335,0.000588435103455636,-0.0110235779195505])
rectify(fp,cam_in,dist_coeff)