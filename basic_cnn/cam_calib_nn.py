# Used for calibration correction, can easily be split into separate files for clarity
import torch
import torch.nn as nn
import torch.nn.functional as F

class tolerance_loss(nn.Module):
    def __init__(self):
        super(tolerance_loss, self).__init__()

    def forward(self, depth_est, depth_gt, mask):
        c = 0.2 #20% tolerance
        d = torch.abs(torch.log(depth_est[mask]) - torch.log(depth_gt[mask]))
        loss = torch.div(d,torch.add(d,c))
        return loss.mean() 

class Network(nn.Module):
    def __init__(self, params, device):
        super(Network, self).__init__()
        self.R = torch.as_tensor(params['R'], device = device) 
        self.t_z = nn.Parameter(torch.tensor(params['t'][2]), requires_grad=False)
        self.t = torch.as_tensor(params['t'], device = device)
        self.t[2] = 0
        self.cx = nn.Parameter(torch.as_tensor(params['K'][2]), requires_grad=True)
        self.cy = nn.Parameter(torch.as_tensor(params['K'][5]), requires_grad=True)
        self.k1 = params['D'][0]
        self.k2 = params['D'][1]
        self.height = params['height']
        self.width = params['width']
        self.scale = params['scale']
        self.fx = nn.Parameter(torch.as_tensor(params['K'][0]), requires_grad=True)
        self.fy = nn.Parameter(torch.as_tensor(params['K'][4]), requires_grad=True)

    def forward(self, data, depth_map):
        vel_to_cam = torch.matmul(self.R, data) + self.t #velodyne to camera frame
        # normalising 3d to 2d
        z = vel_to_cam[:,:,2,:] + self.t_z
        x = vel_to_cam[:,:,0,:]/z 
        y = vel_to_cam[:,:,1,:]/z

        # camera to image to pixel
        u = (self.fx * x + self.cx) * self.scale 
        v = (self.fy * y + self.cy) * self.scale

        # Normalized pixel coords, -1 if on extreme left, 1 if on extreme right 
        u_norm = 2*(u)/(self.width-1) - 1   
        v_norm = 2*(v)/(self.height-1) - 1 
        pixel_coord = torch.stack((u_norm,v_norm),-1)

        # Masking relevant values
        mask = torch.zeros(u.shape, dtype=torch.bool)
        mask[0,:,:] = ((z>0) & (u<self.width) & (v<self.height) & (u>=0) & (v>=0))[0,:,:]
        depth_array = torch.stack((u[mask],v[mask],z[mask]),-1)

        est_depth = F.grid_sample(depth_map,pixel_coord,align_corners=True, mode='bilinear')

        return z, est_depth.squeeze(-2), depth_array.detach().cpu().numpy()

from torch.utils.data import Dataset
import torchvision.transforms.functional as TF
import numpy as np
import os
from PIL import Image
import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import math

class DatasetLoader(Dataset):
    def __init__(self, config):
        filenames_path = config['inputs_path']
        self.data_path = config['data_path']
        self.gt_path = config['gt_path']
        self.lidar_corrected = config['lidar_corrected']
        if config['dataset']=='streetdrone':
            self.lidar_shape = 8
        else:
            self.lidar_shape = 4
        with open(filenames_path, 'r') as f:
            self.filenames = f.readlines()
    
    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        sample_path = self.filenames[idx]

        lidar_path = os.path.join(self.data_path, sample_path.split()[0])
        lidar = np.fromfile(lidar_path, dtype=np.float32)
        depth_path = os.path.join(self.gt_path, sample_path.split()[1])
        depth_gt = Image.open(depth_path)
        img_path = os.path.join(self.data_path, sample_path.split()[2])
        img = cv2.imread(img_path)

        if self.lidar_corrected:
            lidar = lidar.reshape((3,-1))
        else:
            lidar = lidar.reshape((-1, self.lidar_shape))
            lidar = np.transpose(lidar[:,0:3])

        lidar = np.pad(lidar,[(0,0),(0,125000-lidar.shape[1])],'constant') #padding values so all tensors are the same size
        lidar = TF.to_tensor(lidar)

        depth_gt = np.asarray(depth_gt, dtype=np.float32) 
        depth_gt = np.expand_dims(depth_gt, axis=2) / 256.0
        depth_gt = TF.to_tensor(depth_gt)

        sample = {
            'lidar': lidar, 
            'depth': depth_gt,
            'image': img
            }

        return sample

def train(config, params):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    train_dataset = DatasetLoader(config)
    training_loader = torch.utils.data.DataLoader(train_dataset, batch_size=config['batch_size'], shuffle=True)

    model = Network(params, device)
    model.to(device)
    optimizer = optim.AdamW(model.parameters(), lr=config['learning_rate'], weight_decay=config['weight_decay'])
    criterion = tolerance_loss()

    ave_loss = []
    params = []
    for epoch in range(config['epochs']):
        running_loss = 0

        for i, data in enumerate(training_loader):
            # torch.autograd.set_detect_anomaly(True)
            lidar = data['lidar'].to(device)
            depth_gt = data['depth'].to(device)
            img = data['image'][0,:,:,:].numpy().squeeze()

            optimizer.zero_grad() #clear the gradients in model params

            depth_proj, depth_est, depth_array = model(lidar,depth_gt) #forward pass and get predictions

            mask = (depth_proj>0) & (depth_est>0)
            loss = criterion(depth_proj, depth_est, mask)

            loss.backward()
            optimizer.step()

            if torch.isnan(loss).item():
                print('NaN in loss occurred. Aborting training.')
                return -1

            running_loss+=loss.item()
            print("Batch: {}/{}".format(i+1,len(training_loader)))
        print("Epoch: {}/{}, Training Loss: {}".format(epoch+1, config['epochs'], running_loss/len(training_loader)))
        print("cx: {}, cy: {}, fx: {}, fy: {}".format(model.cx.item(), model.cy.item(), model.fx.item(), model.fy.item()))
        print("-"*20)
        ave_loss.append(running_loss/len(training_loader))
        params.append([model.cx.item(), model.cy.item(), model.fx.item(), model.fy.item()])
        checkpoint = {
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict()
            }
        torch.save(checkpoint, '{}_{}.pt'.format('model',epoch+1))

        if config['train_check']:
            img = visual_verification(depth_array, img)
            cv2.imwrite("calib/{:03d}.png".format(epoch+1), img)
    print(ave_loss)
    print(params)
    print("Finished Training and saved the model")

def visual_verification(depth_array, img):
    depth_img = np.array(depth_array[:,2] * 255/depth_array[:,2].max(), dtype = np.uint8) #visual verification 
    depth_map = cv2.applyColorMap(depth_img, cv2.COLORMAP_JET).squeeze()
    img[depth_array[:,1].astype('int32'),depth_array[:,0].astype('int32')]=depth_map

    return img

from os import path, mkdir

def predict(config, params):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    train_dataset = DatasetLoader(config)
    eval_loader = torch.utils.data.DataLoader(train_dataset, batch_size=1)

    model = Network(params, device)
    
    checkpoint = torch.load(config['model_name']+'.pt', map_location=device)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.to(device)
    model.eval()

    pred_depths = []
    imgs = []

    print('Testing files')
    with torch.no_grad():
        for _, sample in enumerate(eval_loader):
            lidar = sample['lidar'].to(device)
            depth_gt = sample['depth'].to(device)
            img = sample['image'][0,:,:,:].numpy().squeeze()

            depth_proj, depth_est, depth_array = model(lidar,depth_gt)

            pred_depths.append(depth_array)
            imgs.append(img)

    print('Done')

    if not path.exists('calib/'+config['name']):
        mkdir('calib/'+config['name'])
    for i in range(len(pred_depths)):
        depth_array = pred_depths[i]
        img = imgs[i]

        img = visual_verification(depth_array, img)
        cv2.imwrite("calib/{}/{:05d}.png".format(config['name'],i+1), img)
    
    if not path.exists('depth/'+config['name']):
        mkdir('depth/'+config['name'])
    for i in range(len(pred_depths)):
        depth_array = pred_depths[i]
        depth = np.zeros((config['height'], config['width']))
        depth[depth_array[:,1].astype('int32'),depth_array[:,0].astype('int32')]=depth_array[:,2]
        depth_data = np.array(depth * 256, dtype = np.uint16) #16 bit depth map
        cv2.imwrite("depth/{}/{:05d}.png".format(config['name'],i+1), depth_data)

import yaml
from torch import optim

if __name__ == '__main__':
    with open('config_sd_cam_calib.yaml') as file:
        config = yaml.full_load(file)

    params = {
        'R': np.array(config['R'],dtype=np.float32).reshape(3,3), 
        't': np.array(config['t'],dtype=np.float32).reshape(3,1), 
        'K': config['K'], 
        'D': config['D'], 
        'scale': config['scale'],
        'height': config['height'], 'width': config['width']}

    train(config, params)
    # predict(config, params)