import sys
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import numpy as np
import matplotlib.pyplot as plt

def compute_errors(gt, pred):
    thresh = np.maximum((gt / pred), (pred / gt))
    d1 = (thresh < 1.25).mean()
    d2 = (thresh < 1.25 ** 2).mean()
    d3 = (thresh < 1.25 ** 3).mean()

    rms = (gt - pred) ** 2
    rms = np.sqrt(rms.mean())

    log_rms = (np.log(gt) - np.log(pred)) ** 2
    log_rms = np.sqrt(log_rms.mean())

    abs_rel = np.mean(np.abs(gt - pred) / gt)
    sq_rel = np.mean(((gt - pred) ** 2) / gt)

    err = np.log(pred) - np.log(gt)
    silog = np.sqrt(np.mean(err ** 2) - np.mean(err) ** 2) * 100

    err = np.abs(np.log10(pred) - np.log10(gt))
    log10 = np.mean(err)

    return [silog, abs_rel, log10, rms, sq_rel, log_rms, d1, d2, d3]

def rolling_window(a, shape):  # rolling window for 2D array
    s = (a.shape[0] - shape[0] + 1,) + (a.shape[1] - shape[1] + 1,) + shape
    strides = a.strides + a.strides
    return np.lib.stride_tricks.as_strided(a, shape=s, strides=strides)

def compute_errors_neighbour(gt, pred, mask):
    #sliding window
    pred=np.pad(pred,1,'constant',constant_values=np.nan)
    windows=rolling_window(pred,(3,3)).reshape(gt.shape +(-1,))
    gt = np.expand_dims(gt[mask],1)
    pred = windows[mask]
    
    #errors
    thresh = np.nanmin(np.maximum((gt / pred), (pred / gt)),1)
    d1 = (thresh < 1.25).mean()
    d2 = (thresh < 1.25 ** 2).mean()
    d3 = (thresh < 1.25 ** 3).mean()

    rms = np.nanmin((gt - pred) ** 2,1)
    rms = np.sqrt(rms.mean())

    log_rms = np.nanmin((np.log(gt) - np.log(pred)) ** 2,1)
    log_rms = np.sqrt(log_rms.mean())

    abs_rel = np.mean(np.nanmin(np.abs(gt - pred) / gt,1))
    sq_rel = np.mean(np.nanmin(((gt - pred) ** 2) / gt,1))

    err = np.nanmin(np.log(pred) - np.log(gt),1)
    silog = np.sqrt(np.mean(err ** 2) - np.mean(err) ** 2) * 100

    err = np.nanmin(np.abs(np.log10(pred) - np.log10(gt)),1)
    log10 = np.mean(err)
    
    return [silog, abs_rel, log10, rms, sq_rel, log_rms, d1, d2, d3]

def evaluate(gt_depths, pred_depths, neighbour=False):
    num_samples = len(gt_depths)

    silog = np.zeros(num_samples, np.float32)
    log10 = np.zeros(num_samples, np.float32)
    rms = np.zeros(num_samples, np.float32)
    log_rms = np.zeros(num_samples, np.float32)
    abs_rel = np.zeros(num_samples, np.float32)
    sq_rel = np.zeros(num_samples, np.float32)
    d1 = np.zeros(num_samples, np.float32)
    d2 = np.zeros(num_samples, np.float32)
    d3 = np.zeros(num_samples, np.float32)

    for i in range(num_samples):
        valid_mask = gt_depths[i]>0
        if neighbour:
            silog[i], abs_rel[i], log10[i], rms[i], sq_rel[i], log_rms[i], d1[i], d2[i], d3[i] = compute_errors_neighbour(gt_depths[i], pred_depths[i], valid_mask)
        else:
            silog[i], abs_rel[i], log10[i], rms[i], sq_rel[i], log_rms[i], d1[i], d2[i], d3[i] = compute_errors(gt_depths[i][valid_mask], pred_depths[i][valid_mask])

        #0-80m cap
        # gt_depth = gt_depths[i]
        # pred_depth = pred_depths[i]
        
        # min_depth_eval = 1e-3
        # max_depth_eval = 80
        # pred_depth[pred_depth < min_depth_eval] = min_depth_eval
        # pred_depth[pred_depth > max_depth_eval] = max_depth_eval
        # pred_depth[np.isinf(pred_depth)] = max_depth_eval
        # pred_depth[np.isnan(pred_depth)] = min_depth_eval

        # valid_mask = np.logical_and(gt_depth > min_depth_eval, gt_depth < max_depth_eval)

        # silog[i], abs_rel[i], log10[i], rms[i], sq_rel[i], log_rms[i], d1[i], d2[i], d3[i] = compute_errors(gt_depth[valid_mask], pred_depth[valid_mask])

    print("{:>7}, {:>7}, {:>7}, {:>7}, {:>7}, {:>7}, {:>7}, {:>7}, {:>7}".format('silog', 'abs_rel', 'log10', 'rms',
                                                                                'sq_rel', 'log_rms', 'd1', 'd2', 'd3'))
    print("{:7.4f}, {:7.4f}, {:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}".format(
        silog.mean(), abs_rel.mean(), log10.mean(), rms.mean(), sq_rel.mean(), log_rms.mean(), d1.mean(), d2.mean(), d3.mean()))
    
def error_map(name, gt_depths, pred_depths, raw):
    num_samples = len(gt_depths)

    for i in range(num_samples):
        valid_mask = gt_depths[i]>0
        err = np.zeros_like(gt_depths[i])

        gt = gt_depths[i][valid_mask]
        pred = pred_depths[i][valid_mask]

        #abs error 
        err[valid_mask] = abs(gt-pred)

        #combined error map - original image & error map
        err_img = np.array(err * 255/err.max(), dtype = np.uint8)
        err_map = cv2.applyColorMap(err_img, cv2.COLORMAP_JET)
        image = cv2.cvtColor(raw[i]*255, cv2.COLOR_RGB2BGR)
        image[valid_mask]=err_map[valid_mask]
        
        cv2.imwrite('error_maps/{}/combined{:05d}.png'.format(name,i+1),image)

def overlay_map(name, pred_depths, raw):
    num_samples = len(pred_depths)

    for i in range(num_samples):
        pred_img = np.array(pred_depths[i] * 255/pred_depths[i].max(), dtype = np.uint8)
        pred_map = cv2.applyColorMap(pred_img, cv2.COLORMAP_JET)
        image = cv2.cvtColor(raw[i]*255, cv2.COLOR_RGB2BGR)

        added_image = cv2.addWeighted(pred_map,1,image,0.7,0, dtype = cv2.CV_32F)

        cv2.imwrite('overlay_maps/{}/overlay{:05d}.png'.format(name,i+1), added_image)

def predict_raw(name, pred_depths):
    for i in range(len(pred_depths)):
        pred_depth_scaled = pred_depths[i] * 256.0
        
        pred_depth_scaled = pred_depth_scaled.astype(np.uint16)
        cv2.imwrite('raw/{}/{:06d}.png'.format(name,i+1), pred_depth_scaled, [cv2.IMWRITE_PNG_COMPRESSION, 0])

def predict_cmap(name, pred_depths):
    for i in range(len(pred_depths)):
        plt.imsave('predictions/{}/predicted{:05d}.png'.format(name,i+1),pred_depths[i], cmap='jet')