import torch
from torch.utils.data import Dataset
from torchvision import transforms
import torchvision.transforms.functional as TF
from PIL import Image
import os
import random
import numpy as np

def transformation(image):
    trans = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean.tolist(), std.tolist())
    ])

    return trans(image)

def transform_pipeline(image, height, width):
    trans = transforms.Compose([
        transforms.ToPILImage(),
        transforms.CenterCrop((height,width)),
        transforms.ToTensor(),
        transforms.Normalize(mean.tolist(), std.tolist())
    ])

    return trans(image)

# normalisation transforms
mean = torch.tensor([0.485, 0.456, 0.406], dtype=torch.float32)
std = torch.tensor([0.229, 0.224, 0.225], dtype=torch.float32)
unnormalise = transforms.Normalize(mean=(-mean / std).tolist(), std=(1.0 / std).tolist()) #mean=-m/s, #std=1/s

colour_jitter = transforms.ColorJitter()

class DepthDataset(Dataset):
    def __init__(self, config, mode):
        if mode == 'train':
            filenames_path = config['train_path']
        else:
            filenames_path = config['test_path']
        self.mode = mode
        self.data_path = config['data_path']
        self.gt_path = config['gt_path']
        self.height = config['height']
        self.width = config['width']
        with open(filenames_path, 'r') as f:
            self.filenames = f.readlines()

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        sample_path = self.filenames[idx]

        image_path = os.path.join(self.data_path, sample_path.split()[0])
        image = Image.open(image_path)

        if self.mode == 'predict':
            image = TF.center_crop(image, (self.height, self.width))
            image = transformation(image)

            sample = {'image': image}

            return sample

        depth_path = os.path.join(self.gt_path, sample_path.split()[1])
        depth_gt = Image.open(depth_path)

        #preprocessing and transforming data 
        if self.mode == 'train':
            image, depth_gt = self.train_preprocess(image, depth_gt)
        else:
            image = TF.center_crop(image, (self.height, self.width))
            depth_gt = TF.center_crop(depth_gt, (self.height, self.width))

        depth_gt = np.asarray(depth_gt, dtype=np.float32) 
        depth_gt = np.expand_dims(depth_gt, axis=2) / 256.0

        # Transform to tensor
        image = transformation(image)
        depth_gt = TF.to_tensor(depth_gt)

        sample = {
            'image': image, 
            'depth': depth_gt
            }

        return sample

    def train_preprocess(self, image, depth):
        # Random crop
        i, j, h, w = transforms.RandomCrop.get_params(
            image, output_size=(self.height, self.width))
        image = TF.crop(image, i, j, h, w)
        depth = TF.crop(depth, i, j, h, w)

        # Random horizontal flipping
        if random.random() > 0.5:
            image = TF.hflip(image)
            depth = TF.hflip(depth)

        # Random vertical flipping
        if random.random() > 0.5:
            image = TF.vflip(image)
            depth = TF.vflip(depth)

        # Colour jitter to image only
        image = colour_jitter(image)

        return image, depth
