from model import Network, inv_huber_loss, tolerance_loss, neighbour_inv_huber_loss
from dataloader import DepthDataset, unnormalise
import evaluation as evals
import torch
from torch import optim

import yaml
from os import path, mkdir
import time

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

class DepthEstimate():
    def __init__(self, config=None):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.config = config
        self.MODEL_PATH = 'models/'+self.config['model_name']

    def train(self):
        train_dataset = DepthDataset(self.config, 'train')
        training_loader = torch.utils.data.DataLoader(train_dataset, batch_size=self.config['batch_size'], shuffle=True)

        model = Network()
        optimizer = optim.AdamW(model.parameters(), lr=self.config['learning_rate'], weight_decay=self.config['weight_decay'])
        if self.config['dataset']=='streetdrone':
            criterion = neighbour_inv_huber_loss()
        else:
            criterion = inv_huber_loss()

        #load model
        model.to(self.device)
        model, optimizer = self.load_state_dict(model, optimizer)
        model.train()
        self.fixing_layers(model)

        ave_loss = []
        for epoch in range(self.config['epochs']):
            running_loss = 0

            for i, data in enumerate(training_loader):
                image = data['image'].to(self.device)
                depth_gt = data['depth'].to(self.device)

                optimizer.zero_grad() #clear the gradients in model params

                depth_est = model(image) #forward pass and get predictions

                mask = depth_gt > 0
                loss = criterion.forward(depth_est, depth_gt, mask.to(torch.bool))

                loss.backward()
                optimizer.step()

                if torch.isnan(loss).item():
                    print('NaN in loss occurred. Aborting training.')
                    return -1

                running_loss+=loss.item()
                print("Batch: {}/{}".format(i+1,len(training_loader)))
            print("Epoch: {}/{}, Training Loss: {}".format(epoch+1, self.config['epochs'], running_loss/len(training_loader)))
            print("-"*20)
            ave_loss.append(running_loss/len(training_loader))
            checkpoint = {
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict()
                }
            torch.save(checkpoint, '{}_{}.pt'.format(self.MODEL_PATH,epoch+1))
        print(ave_loss)
        print("Finished Training and saved the model")

    def predict(self):
        #load dataset
        test_dataset = DepthDataset(self.config, 'predict')
        eval_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1)

        #load model
        model = Network()
        model = self.load_state_dict(model)[0]
        model.to(self.device)
        model.eval()

        pred_depths = []

        start_time = time.time()
        print('Testing files')
        with torch.no_grad():
            for _, sample in enumerate(eval_loader):
                image = sample['image'].to(self.device)

                pred_depth = model(image)

                pred_depths.append(pred_depth.cpu().numpy().squeeze())
        elapsed_time = time.time() - start_time
        print('Elapsed time: %s' % str(elapsed_time))
        print('Done')

        #scaled predictions
        print('Saving raw predictions pngs')
        if not path.exists('raw/'+self.config['name']):
            mkdir('raw/'+self.config['name'])
        evals.predict_raw(self.config['name'], pred_depths)

    def test(self):
        #load dataset
        test_dataset = DepthDataset(self.config, 'test')
        eval_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1)

        #load model
        model = Network()
        model = self.load_state_dict(model)[0]
        model.to(self.device)
        model.eval()

        pred_depths = []
        gt_depths = []
        raw = []
        
        print('Parameters: %s' % str(count_parameters(model)))
        # print(model.decoder.offset) #checking offset values

        start_time = time.time()
        print('Testing files')
        with torch.no_grad():
            for _, sample in enumerate(eval_loader):
                image = sample['image'].to(self.device)
                gt_depth = sample['depth'].to(self.device)

                pred_depth = model(image)

                pred_depths.append(pred_depth.cpu().numpy().squeeze())
                gt_depths.append(gt_depth.cpu().numpy().squeeze())
                if self.config['error_maps'] or self.config['overlay_maps']:
                    image = unnormalise(image.squeeze()) #restore original colours
                    raw.append(image.cpu().permute(1, 2, 0).numpy())
        elapsed_time = time.time() - start_time
        print('Elapsed time: %s' % str(elapsed_time))
        print('Done')

        #compute errors
        print('Computing errors')
        if self.config['dataset'] == 'streetdrone':
            evals.evaluate(gt_depths, pred_depths, True)
        else:
            evals.evaluate(gt_depths, pred_depths)

        #save error maps
        if self.config['error_maps']:
            print('Saving error maps')
            if not path.exists('error_maps/'+self.config['name']):
                mkdir('error_maps/'+self.config['name'])
            evals.error_map(self.config['name'], gt_depths, pred_depths, raw)

        #save images
        print('Saving result pngs')
        if not path.exists('predictions/'+self.config['name']):
            mkdir('predictions/'+self.config['name'])
        evals.predict_cmap(self.config['name'], pred_depths)

        #combine prediction + raw
        if self.config['overlay_maps']:
            print('Saving overlay maps')
            if not path.exists('overlay_maps/'+self.config['name']):
                mkdir('overlay_maps/'+self.config['name'])
            evals.overlay_map(self.config['name'], pred_depths,raw)

    def load_state_dict(self, model, optimizer=None):
        if path.isfile(self.MODEL_PATH+'.pt'):
            checkpoint = torch.load(self.MODEL_PATH+'.pt', map_location=self.device)
            model.load_state_dict(checkpoint['model_state_dict'])
            if optimizer != None:
                optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            print("Loaded model state dict")
        return model, optimizer

    def fixing_layers(self, model):
        if self.config['dataset'] == 'streetdrone':  
            training_layers = ['transition1','transition2','offset']
            for name, child in model.named_children():
                for name2, param in child.named_parameters():
                    # print(name, name2)
                    if any(x in name2 for x in training_layers):
                        print(name, name2)
                    else:
                        param.requires_grad = False 

if __name__ == '__main__':
    with open('config.yaml') as file:
        config = yaml.full_load(file)
    depthest=DepthEstimate(config)
    # depthest.train()
    depthest.test()
    # depthest.predict()